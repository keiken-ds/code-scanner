<?php
  session_start(); 

  if (!isset($_SESSION['username'])) {
        $_SESSION['msg'] = "You must log in first";
        header('location: /');
  }
  if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION['username']);
        header("location: /");
  }
?>
<!DOCTYPE html>
<html>
<head>
        <title>Home</title>
        <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div class="header">
        <h2>Home Page</h2>
        <p class="logout">
                <a href="index.php?logout='1'" style="color: red;">logout</a>
        </p>

</div>
<div class="content">
        <!-- notification message -->
        <?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
        <h3>
          <?php 
                echo $_SESSION['success']; 
                unset($_SESSION['success']);
          ?>
        </h3>
      </div>
        <?php endif ?>

    <!-- logged in user information -->
         <?php  if (isset($_SESSION['username'])) : ?>
                <p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
                <?php
                        $query = $_SERVER['QUERY_STRING'];
                        if(!(substr_count($query, '_') > 0) && !(substr_count($query, '%')> 1)){
                                $clean = 1;}
                        else{
                                $clean = 0;}
                        if($_SESSION['username'] === "admin" || (($_GET["a_d_m_i_n"] === "secret") && $clean)) :?>
                        <p><br>
                                <form method="GET" action="index.php">
                                        Enter pass to see secret note
                                        <input type="password" name="note_pass">
                                </form>
                                <br><br>
                                <?php
                                        $pass = $_GET['note_pass'];
                                        $match = "/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/i";
                                        if (preg_match($match,$pass)) :?>
						echo "my secret note";
					<?php endif ?>
                	<?php endif ?>
			</p>
        <?php endif ?>
</div>
</body>
</html>
